# README #

**Training project(code sample). Simple 2d game.**
*Some SDK keys were removed or commented*

### Technologies used ###

* Swift 2.2 language
* SpriteKit engine

### Screenshot ###
![Screenshot.jpg](https://bitbucket.org/repo/yBbxaB/images/1363624352-screenshot-demo-smallres.jpg)
[https://appsto.re/ua/vY3Udb.i](https://appsto.re/ua/vY3Udb.i)